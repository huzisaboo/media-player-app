package com.huzaifa.g2mediaplayerapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    RecyclerView myRecyclerView;
    PlayerAdapter playerAdapter;
    public static int REQUEST_PERMISSION = 1;
    boolean bool_permission;
    public static ArrayList<File> fileArrayList = new ArrayList<>();
    File dir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myRecyclerView = (RecyclerView) findViewById(R.id.video_list);

        //Retrieves from both, Phone and SD storage for some reason
        dir = new File("/sdcard/");

        GridLayoutManager gridLayoutManager = new GridLayoutManager(MainActivity.this,2);
        myRecyclerView.setLayoutManager(gridLayoutManager);

        permissionForVideo();

    }

    private void permissionForVideo() {
        if(ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            if(ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,Manifest.permission.READ_EXTERNAL_STORAGE)){
            }
            else
            {
                ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},REQUEST_PERMISSION);
            }
        }
        else
        {
            bool_permission = true;
            getFile(dir);
            playerAdapter = new PlayerAdapter(getApplicationContext(),fileArrayList);
            myRecyclerView.setAdapter(playerAdapter);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == REQUEST_PERMISSION)
        {
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                bool_permission = true;
                getFile(dir);
                playerAdapter = new PlayerAdapter(getApplicationContext(),fileArrayList);
                myRecyclerView.setAdapter(playerAdapter);
            }
            else
            {
                Toast.makeText(this, "Please Allow Permission!",Toast.LENGTH_SHORT).show();
            }
        }

    }

    public ArrayList<File> getFile(File dir) {
        File listFile[] = dir.listFiles();
        if(listFile != null && listFile.length > 0)
        {
            for(int i=0; i<listFile.length;i++)
            {
                if(listFile[i].isDirectory())
                {
                    getFile(listFile[i]);
                }
                else
                {
                    bool_permission = false;
                    if(listFile[i].getName().endsWith(".mp4"))
                    {
                        for(int j = 0; j<fileArrayList.size();j++)
                        {
                            if(fileArrayList.get(j).getName().equals(listFile[i].getName()))
                            {
                                bool_permission = true;
                            }
                        }
                        if(bool_permission)
                        {
                            bool_permission = false;
                        }
                        else
                        {
                            fileArrayList.add(listFile[i]);
                        }
                    }
                }
            }
        }
        return fileArrayList;
    }
}
