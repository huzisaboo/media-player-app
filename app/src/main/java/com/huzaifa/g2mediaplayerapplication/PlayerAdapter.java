package com.huzaifa.g2mediaplayerapplication;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.io.File;
import java.util.ArrayList;

public class PlayerAdapter extends RecyclerView.Adapter<VideoHolder> {

    private Context context;
    ArrayList<File> videoList;

    public PlayerAdapter(Context context, ArrayList<File> videoList) {
        this.context = context;
        this.videoList = videoList;
    }

    @Override
    public VideoHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View myView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.video_item,viewGroup,false);
        return new VideoHolder(myView);
    }

    @Override
    public void onBindViewHolder(@NonNull VideoHolder holder, int position) {

        holder.textViewFile.setText(MainActivity.fileArrayList.get(position).getName());
        Bitmap thumbnail = ThumbnailUtils.createVideoThumbnail(videoList.get(position).getPath(),MediaStore.Images.Thumbnails.FULL_SCREEN_KIND);
        holder.imgThumbnail.setImageBitmap(thumbnail);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context,  VideoPlayerActivity.class);
                intent.putExtra("position",holder.getAdapterPosition());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        if(!videoList.isEmpty())
        {
            return videoList.size();
        }
        else
        {
            return 1;
        }
    }
}

class VideoHolder extends RecyclerView.ViewHolder
{

    TextView textViewFile;
    ImageView imgThumbnail;
    CardView cardView;

    VideoHolder(View view)
    {
        super(view);
        textViewFile = view.findViewById(R.id.view_fileName);
        imgThumbnail = view.findViewById(R.id.view_thumbnail);
        cardView = view.findViewById(R.id.view_cardView);
    }

}
